package jsons

import (
	"bytes"
	"encoding/json"
)

// JSONString 将目标对象转为字符串
func JSONString(v interface{}) string {
	return string(JSONByte(v))
}

func JSONByte(v interface{}) []byte {
	jsonStu, err := json.Marshal(v)
	if err != nil {
		return []byte("null")
	}
	return jsonStu
}

// ParseObject 将json字符的字节数组反序列化成对象
func ParseObject(jsonByte []byte, v interface{}) error {
	decoder := json.NewDecoder(bytes.NewBuffer(jsonByte))
	decoder.UseNumber()
	err := decoder.Decode(v)
	//err := json.Unmarshal(jsonByte, v)
	if err != nil {
		return err
	}
	return nil
}

// ParseObjectFromString 将json字节数组反序列化成对象
func ParseObjectFromString(jsonStr string, v interface{}) error {
	return ParseObject([]byte(jsonStr), v)
}

// JSONConvert 将data转成result对象
func JSONConvert(data, result interface{}) error {
	return ParseObject(JSONByte(data), &result)
}

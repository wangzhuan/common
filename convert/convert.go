package convert

import (
	"fmt"
	"log"
	"reflect"
	"runtime"
	"strconv"
)

func AnyToBool(target interface{}) bool {
	ret := false
	v := reflectValueOf(target)
	switch v.Kind() {
	case reflect.Int, reflect.Int8, reflect.Int16, reflect.Int32, reflect.Int64:
		ret = v.Int() > 0
	case reflect.Uint, reflect.Uint8, reflect.Uint16, reflect.Uint32, reflect.Uint64:
		ret = v.Uint() > 0
	case reflect.String:
		ret, _ = strconv.ParseBool(v.String())
	case reflect.Float32, reflect.Float64:
		ret = v.Float() > 0
	case reflect.Bool:
		ret = v.Bool()
	}
	return ret
}

func AnyToString(target interface{}) string {
	ret := ""
	v := reflectValueOf(target)
	switch v.Kind() {
	case reflect.Int, reflect.Int8, reflect.Int16, reflect.Int32, reflect.Int64:
		i := v.Int()
		ret = strconv.FormatInt(i, 10)
	case reflect.Uint, reflect.Uint8, reflect.Uint16, reflect.Uint32, reflect.Uint64:
		i := v.Uint()
		ret = strconv.FormatUint(i, 10)
	case reflect.String:
		ret = v.String()
	case reflect.Float32:
		f := v.Float()
		//不能共用float64,否则转换为字符串会多出很多小数
		ret = strconv.FormatFloat(f, 'f', -1, 32)
	case reflect.Float64:
		f := v.Float()
		ret = strconv.FormatFloat(f, 'f', -1, 64)
	case reflect.Bool:
		ret = strconv.FormatBool(v.Bool())
	}
	return ret
}

func StringToInt64(target string) int64 {
	parseInt, err := strconv.ParseInt(target, 10, 64)
	if err != nil {
		_, file, line, _ := runtime.Caller(1)

		log.Printf("string  => int  fail,target:%v,err:%v,callFile:%v,callLine:%v", target, err, file, line)
		return 0
	}
	return parseInt
}

func StringToUint64(target string) uint64 {
	parseInt, err := strconv.ParseUint(target, 10, 64)
	if err != nil {
		_, file, line, _ := runtime.Caller(1)
		log.Printf("string => int fail,target:%v,err:%v,callFile:%v,callLine:%v", target, err, file, line)
		return 0
	}
	return parseInt
}

func StringToFloat64(str string) float64 {
	parseInt, err := strconv.ParseFloat(str, 64)
	if err != nil {
		_, file, line, _ := runtime.Caller(1)
		log.Printf("string mid => float mid fail,mid:%v,err:%v,callFile:%v,callLine:%v", str, err, file, line)
		return 0
	}
	return parseInt
}

func Int64ToString(mid int64) string {
	return fmt.Sprintf("%d", mid)
}

func reflectValueOf(target interface{}) reflect.Value {
	v := reflect.ValueOf(target)
	kind := v.Kind()
	if kind == reflect.Ptr {
		v = v.Elem()
	}
	return v
}

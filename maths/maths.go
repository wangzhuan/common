package maths

import (
	"math"
	"math/rand"
)

// Round 四舍五入
// in 被处理的小数
// a 保留小数的位数
func Round(in float64, a int) float64 {
	pow10 := math.Pow10(a)
	return math.Round(in*pow10) / pow10
}

func MaxInt64(one, two int64) int64 {
	if one > two {
		return one
	}
	return two
}

type number interface {
	int | int8 | int16 | int32 | int64 | float32 | float64 | uint | uint8 | uint16 | uint32 | uint64
}

// Rand 计算指定范围内的随机数
func Rand[T number](min T, max T) T {
	tmp := float64(min) + (float64(max)-float64(min))*rand.Float64()
	return T(tmp)
}

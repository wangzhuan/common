package vals

import (
	"fmt"
	"gitlab.com/wangzhuan/common/mix/vals/cond"
	"gitlab.com/wangzhuan/common/mix/vals/data"
	"testing"
)

type packetCalcItem struct {
	DownturnCoefficient float64                    `json:"downturn_coefficient"`
	PacketAmount        *data.ValueMinMax[float64] `json:"packet_amount"`
	PreviewAmount       *data.ValueMinMax[float64] `json:"preview_amount"`
}

var PacketCalcConfig2 = ConfigList[float64, string, cond.MinMax[float64]]{
	{Scope: cond.MinMax[float64]{Min: 0, Max: 1, M: cond.L}, Data: "L"},
	{Scope: cond.MinMax[float64]{Min: 2, Max: 3}, Data: "R"},
	{Scope: cond.MinMax[float64]{Min: 4, Max: 5, M: cond.N}, Data: "N"},
	{Scope: cond.MinMax[float64]{Min: 6, Max: 7, M: cond.B}, Data: "B"},
	{Scope: cond.MinMax[float64]{Min: 1000, M: cond.GTE}, Data: "GTE"},
	{Scope: cond.MinMax[float64]{Min: 100, M: cond.GT}, Data: "GT"},
	{Scope: cond.MinMax[float64]{Max: -100, M: cond.LTE}, Data: "LTE"},
	{Scope: cond.MinMax[float64]{Max: -1, M: cond.LT}, Data: "LT"},
}

var PacketCalcConfig = ConfigList[float64, *packetCalcItem, cond.MinMax[float64]]{
	{Scope: cond.MinMax[float64]{Min: -0.01, Max: 66.00}, Data: &packetCalcItem{DownturnCoefficient: 1.00, PacketAmount: &data.ValueMinMax[float64]{Min: 0.8000, Max: 1.0000}, PreviewAmount: &data.ValueMinMax[float64]{Min: 7.0000, Max: 8.0000}}},
	{Scope: cond.MinMax[float64]{Min: 66.00, Max: 75.00}, Data: &packetCalcItem{DownturnCoefficient: 4.00, PacketAmount: &data.ValueMinMax[float64]{Min: 0.8000, Max: 1.0000}, PreviewAmount: &data.ValueMinMax[float64]{Min: 7.0000, Max: 8.0000}}},
	{Scope: cond.MinMax[float64]{Min: 75.00, Max: 80.00}, Data: &packetCalcItem{DownturnCoefficient: 3.00, PacketAmount: &data.ValueMinMax[float64]{Min: 0.8000, Max: 1.0000}, PreviewAmount: &data.ValueMinMax[float64]{Min: 5.0000, Max: 6.0000}}},
	{Scope: cond.MinMax[float64]{Min: 80.00, Max: 85.00}, Data: &packetCalcItem{DownturnCoefficient: 2.00, PacketAmount: &data.ValueMinMax[float64]{Min: 0.8000, Max: 1.0000}, PreviewAmount: &data.ValueMinMax[float64]{Min: 3.0000, Max: 4.0000}}},
	{Scope: cond.MinMax[float64]{Min: 85.00, Max: 90.00}, Data: &packetCalcItem{DownturnCoefficient: 1.50, PacketAmount: &data.ValueMinMax[float64]{Min: 0.8000, Max: 1.0000}, PreviewAmount: &data.ValueMinMax[float64]{Min: 2.0000, Max: 3.0000}}},
	{Scope: cond.MinMax[float64]{Min: 90.00, Max: 92.00}, Data: &packetCalcItem{DownturnCoefficient: 1.00, PacketAmount: &data.ValueMinMax[float64]{Min: 0.8000, Max: 1.0000}, PreviewAmount: &data.ValueMinMax[float64]{Min: 1.5000, Max: 2.0000}}},
	{Scope: cond.MinMax[float64]{Min: 92.00, Max: 93.00}, Data: &packetCalcItem{DownturnCoefficient: 0.75, PacketAmount: &data.ValueMinMax[float64]{Min: 0.8000, Max: 1.0000}, PreviewAmount: &data.ValueMinMax[float64]{Min: 1.0000, Max: 1.5000}}},
	{Scope: cond.MinMax[float64]{Min: 93.00, Max: 97.00}, Data: &packetCalcItem{DownturnCoefficient: 0.50, PacketAmount: &data.ValueMinMax[float64]{Min: 0.8000, Max: 1.0000}, PreviewAmount: &data.ValueMinMax[float64]{Min: 0.5000, Max: 1.5000}}},
	{Scope: cond.MinMax[float64]{Min: 97.00, Max: 99.00}, Data: &packetCalcItem{DownturnCoefficient: 0.30, PacketAmount: &data.ValueMinMax[float64]{Min: 0.8000, Max: 1.0000}, PreviewAmount: &data.ValueMinMax[float64]{Min: 0.5000, Max: 1.0000}}},
	{Scope: cond.MinMax[float64]{Min: 99.00, Max: 99.30}, Data: &packetCalcItem{DownturnCoefficient: 0.10, PacketAmount: &data.ValueMinMax[float64]{Min: 0.8000, Max: 1.0000}, PreviewAmount: &data.ValueMinMax[float64]{Min: 0.3000, Max: 0.5000}}},
	{Scope: cond.MinMax[float64]{Min: 99.30, Max: 99.90}, Data: &packetCalcItem{DownturnCoefficient: 1.00, PacketAmount: &data.ValueMinMax[float64]{Min: 0.0100, Max: 0.0100}, PreviewAmount: &data.ValueMinMax[float64]{Min: 0.1000, Max: 0.2000}}},
	{Scope: cond.MinMax[float64]{Min: 99.90, Max: 99.99}, Data: &packetCalcItem{DownturnCoefficient: 1.00, PacketAmount: &data.ValueMinMax[float64]{Min: 0.0010, Max: 0.0010}, PreviewAmount: &data.ValueMinMax[float64]{Min: 0.1000, Max: 0.2000}}},
	{Scope: cond.MinMax[float64]{Min: 99.99, Max: 100.0}, Data: &packetCalcItem{DownturnCoefficient: 1.00, PacketAmount: &data.ValueMinMax[float64]{Min: 0.0001, Max: 0.0001}, PreviewAmount: &data.ValueMinMax[float64]{Min: 0.1000, Max: 0.2000}}},
}

func TestConfigList_Get(t *testing.T) {
	fmt.Println(0, PacketCalcConfig2.Get(0))
	fmt.Println(1, PacketCalcConfig2.Get(1))
	fmt.Println(2, PacketCalcConfig2.Get(2))
	fmt.Println(3, PacketCalcConfig2.Get(3))
	fmt.Println(4, PacketCalcConfig2.Get(4))
	fmt.Println(4.5, PacketCalcConfig2.Get(4.5))
	fmt.Println(5, PacketCalcConfig2.Get(5))
	fmt.Println(6, PacketCalcConfig2.Get(6))
	fmt.Println(7, PacketCalcConfig2.Get(7))
	fmt.Println(999.9999, PacketCalcConfig2.Get(999.9999))
	fmt.Println(1000, PacketCalcConfig2.Get(1000))
	fmt.Println(1000.001, PacketCalcConfig2.Get(1000.001))
	fmt.Println(100, PacketCalcConfig2.Get(100))
	fmt.Println(100.001, PacketCalcConfig2.Get(100.001))
	fmt.Println(-99.9999, PacketCalcConfig2.Get(-99.9999))
	fmt.Println(-100, PacketCalcConfig2.Get(-100))
	fmt.Println(-100.001, PacketCalcConfig2.Get(-100.001))
	fmt.Println(-1, PacketCalcConfig2.Get(-1))
	fmt.Println(-1.001, PacketCalcConfig2.Get(-1.001))
	//fmt.Println(PacketCalcConfig.Get(-9).PacketAmount)
	c, d := PacketCalcConfig.GetScopeAndData(1)
	fmt.Println(d.PacketAmount.Value())
	fmt.Println(c.Max)
}

package vals

import (
	"gitlab.com/wangzhuan/common/mix/vals/cond"
)

// Config 单独的一个配置，包含范围和数据两部分内容
type Config[C cond.ConditionType, D any, S cond.Condition[C]] struct {
	Scope S `json:"scope"`
	Data  D `json:"data"`
}

// ConfigList 一系列的配置，用来从中匹配合适的配置
type ConfigList[C cond.ConditionType, D any, S cond.Condition[C]] []Config[C, D, S]

func (i ConfigList[C, D, S]) Get(condition C) D {
	var d D
	return i.GetOrDefault(condition, d)
}

func (i ConfigList[C, D, S]) GetOrDefault(condition C, defaultValue D) D {
	_, d := i.GetScopeAndDataOrDefault(condition, defaultValue)
	return d
}

func (i ConfigList[C, D, S]) GetScopeAndData(condition C) (S, D) {
	var d D
	return i.GetScopeAndDataOrDefault(condition, d)
}

func (i ConfigList[C, D, S]) GetScopeAndDataOrDefault(condition C, defaultValue D) (S, D) {
	if i == nil {
		var s S
		return s, defaultValue
	}
	for _, item := range i {
		if item.Scope.Match(condition) {
			return item.Scope, item.Data
		}
	}
	var s S
	return s, defaultValue
}

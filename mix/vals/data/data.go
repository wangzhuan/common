package data

import (
	"gitlab.com/wangzhuan/common/maths"
	"gitlab.com/wangzhuan/common/mix/vals/cond"
)

type Model string

const (
	L Model = ""      //默认方式[min,max)
	R Model = "Right" //(min,max]
	//N Model = "None"  //(min,max)
	//B Model = "Both"  //[min,max]
)

// ValueMinMax 根据最大值最小值返回一个随机值
type ValueMinMax[T cond.Number] struct {
	Min T     `json:"min"`
	Max T     `json:"max"`
	M   Model `json:"m"`
}

// Value 返回一个随机值，范围为[min,max)
func (v ValueMinMax[T]) Value() T {
	switch v.M {
	case R:
		return v.Max - maths.Rand(v.Min, v.Max)
	}
	return maths.Rand(v.Min, v.Max)
}

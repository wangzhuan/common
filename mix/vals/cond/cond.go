package cond

// 条件，用来根据一个值判断是否在当前范围内
type Condition[C any] interface {
	Match(num C) bool
}

type Number interface {
	int | int8 | int16 | int32 | int64 | float32 | float64 | uint | uint8 | uint16 | uint32 | uint64
}
type ConditionType interface {
	Number | string
}

type Model string

const (
	R   Model = ""     //默认方式;(min,max]
	N   Model = "None" //(min,max)
	L   Model = "Left" //[min,max)
	B   Model = "Both" //[min,max]
	GT  Model = "GT"   //(min,∞)
	GTE Model = "GTE"  //[min,∞)
	LT  Model = "LT"   //(∞,max)
	LTE Model = "LTE"  //(∞,max]
)

// MinMax 实现了scope接口，用来判断是否在两个数之间
type MinMax[T Number] struct {
	Min T     `json:"min"`
	Max T     `json:"max"`
	M   Model `json:"m"`
}

func (m MinMax[number]) Match(v number) bool {
	switch m.M {
	case N:
		return m.Min < v && v < m.Max
	case L:
		return m.Min <= v && v < m.Max
	case B:
		return m.Min <= v && v <= m.Max
	case GT:
		return m.Min < v
	case GTE:
		return m.Min <= v
	case LT:
		return v < m.Max
	case LTE:
		return v <= m.Max
	}
	//默认处理方式为m.M==R
	if m.Min < v && v <= m.Max {
		return true
	}
	return false
}

// Eq 实现了scope接口，用来判断是否与配置数据相等
type Eq[T ConditionType] struct {
	Eq T `json:"eq"`
}

func (m Eq[conditionType]) Match(v conditionType) bool {
	return m.Eq == v
}

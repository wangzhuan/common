package aes

import (
	"encoding/base64"
	"fmt"
	"testing"
)

func TestAesEncryptCBCAndBase64(t *testing.T) {
	mingwen := "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"
	key := "12345678901234567890123456789212"
	iv := "1234567890123456"

	encrypted := EncryptCBCAndBase64(mingwen, key, iv)
	fmt.Println(encrypted)

	fmt.Println(DecryptCBCByBase64(encrypted, key, iv))

	decodeString, _ := base64.StdEncoding.DecodeString(encrypted)
	fmt.Println("明文长度", len([]byte(mingwen)))
	fmt.Println("密文长度", len(decodeString))

}

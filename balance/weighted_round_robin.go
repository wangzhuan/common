package balance

type item struct {
	weight        int
	currentWeight int
}

type WeightedRoundRobin struct {
	items       []*item
	totalWeight int
}

func (b *WeightedRoundRobin) Next() int {
	maxWeight := 0
	result := 0
	for index, i := range b.items {
		i.currentWeight += i.weight
		if i.currentWeight > maxWeight {
			maxWeight = i.currentWeight
			result = index
		}
	}

	b.items[result].currentWeight -= b.totalWeight
	return result
}

func NewWeightedRoundRobin(weightList []int) *WeightedRoundRobin {
	r := &WeightedRoundRobin{
		items:       []*item{},
		totalWeight: 0,
	}
	for i := 0; i < len(weightList); i++ {
		r.items = append(r.items, &item{
			weight:        weightList[i],
			currentWeight: 0,
		})
		r.totalWeight += weightList[i]
	}
	return r
}
